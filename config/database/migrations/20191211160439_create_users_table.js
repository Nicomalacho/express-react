
exports.up = function(knex) {
  return knex.schema.createTable('users', (table) => {
    table.string('id').primary();
    table.string('email_address').notNull();
    table.string('password').notNull();        
    table.timestamps();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('users');
};
