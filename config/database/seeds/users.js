exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          id: 1,
          email_address: "test@gmail.com",
          password: "$2b$10$Hgdubyff7dyznKjkzrIiJ.aG2ti0gqzmIBBt.rpEGZAfZIEHYadsG",
        },
      ]);
    });
};