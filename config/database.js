module.exports = {
  development: setupConfig(),
  staging: setupConfig(),
  test: setupConfig(),
  production: setupConfig(),
};

function setupConfig(environment) {
  const env = environment || process.env;
  if (env.NODE_ENV === 'test') {
    return { client: 'mysql', debug: false };
  }

  return {
    client: env.DB_CLIENT || 'mysql',
    debug: env.DB_DEBUG || false,
    connection: {
      host: env.DB_HOST || '127.0.0.1',
      port: env.DB_PORT || '3306',
      user: env.DB_USER || 'coquito_dev_user',
      password: env.DB_PWD || 'coquito_dev_p4ssw0rd',
      database: env.DB_NAME || 'coquito_dev',
      charset: env.DB_CHARSET || 'utf8',
      reconnect: env.DB_RECONNECT || true,
      timezone: env.DB_TZ || 'UTC',
      ssl: env.DB_SSL || false,
    },
    pool: {
      min: parseInt(env.DB_POOL_MIN, 10) || 1,
      max: parseInt(env.DB_POOL_MAX, 10) || 5,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './database/migrations',
    },
    seeds: {
      directory: './database/seeds',
    },
  };
}
