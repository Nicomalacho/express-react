import database from './database';
export default (environment) => {
  const jwt = {
    issuer: environment.DOMAIN || 'test.coquito.co',
  };
  const ENV = {
    environment: environment.NODE_ENV || 'development',
    app: {
      version: '1.0',
      baseURL: '/v1',
      defaultPort: '5000',
      secretOrKey: environment.API_SECRET || 'mdSuTSj4F9CnuSKz6hUOLtOtak5rwgvxjmdyM8275atlDG37RWsXeunYRAGeKMn',
      loginExpiresIn: '1h',
      jwt,      
    },
    coquito: {
      verificationTokenExpiresIn: '365d',
      resetPasswordTokenExpiresIn: '3h',
    },
    bodyParser: {
      json: { limit: '100kb' },
      urlencoded: { extended: false },
    },
  }
  ENV.coquito.database = database[ENV.environment];
  return ENV;
}