export default class {
  constructor(context) {
    this.context = context;
    this.config = context.config;
    this.buckets = context.buckets;
  }  
}
