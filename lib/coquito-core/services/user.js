import userActions from './user/index';
import BaseService from './base';
import utils from '../utils';

export default class UserService extends BaseService {
  constructor(context) {
    super(context);
    this.userBucket = this.buckets.user;
  }
  async find(id) {
    try {
      const user = await this.userBucket.findOne({ id });
      return utils.parseData(user);
    } catch (e) {
      throw e;
    }
  }
  async login(data) {
    const { emailAddress, password } = data;
    try {
      return await userActions.login(this, emailAddress, password);
    } catch (e) {
      throw e;
    }
  }
}
