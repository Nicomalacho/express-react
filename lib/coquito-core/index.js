import UserService from './services/user';

export default function (buckets, config) {
  const context = {
    config,
    buckets,
  };
  const services = {};
  services.user = new UserService(context);
  return services;
}
