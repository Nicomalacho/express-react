
export default {
  tableName: 'users',
  idAttribute: 'id',
  hidden: ['password'],
  hasTimestamps: true,
  queries: {
    findByEmail(emailAddress){      
      return this.findOne({ emailAddress }, { require })
    }
  }
}