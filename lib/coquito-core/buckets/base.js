import Knex from 'knex';
import Bookshelf from 'bookshelf';
import modelBase from 'bookshelf-modelbase';
import modelBasePlus from 'bookshelf-modelbase-plus';
import relationships from 'bookshelf-relationships';

export default function (config) {
  const knex = Knex(config);
  const bookshelf = Bookshelf(knex);

  // bookshelf.plugin('virtuals');
  bookshelf.plugin([
    'bookshelf-camelcase',
    'bookshelf-jsonapi-params',
    'bookshelf-schema',
  ]);

  bookshelf.plugin(modelBase.pluggable);
  bookshelf.plugin(modelBasePlus);
  bookshelf.plugin(relationships);

  return {
    knex,
    bookshelf,
    Model: bookshelf.Model,
    Collection: bookshelf.Collection,
  };
}
