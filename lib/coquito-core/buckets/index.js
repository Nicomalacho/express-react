import BaseBucket from './base';
import user from './user';
export default function(config) {
  const dbConfig = config.database;
  const buckets = {};
  const base = BaseBucket(dbConfig);
  buckets.user = buildBucket(base, user);

  buckets.connection = base.knex;

  return buckets;
}
function buildBucket(base, options) {
  return base.Model.extend(options, options.queries);
}
