import http from 'http';
import app from '../app/app';

const port = normalizePort(process.env.PORT || app.locals.appContext.config.app.defaultPort);
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => {
  /* run after server started */
});

server.on('error', onError);

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      // eslint-disable-next-line no-console
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      // eslint-disable-next-line no-console
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
* Normalize a port into a number, string, or false.
*/

function normalizePort(val) {
  const nPort = parseInt(val, 10);

  if (Number.isNaN(nPort)) {
    // named pipe
    return val;
  }

  if (nPort >= 0) {
    // nPort number
    return nPort;
  }

  return false;
}