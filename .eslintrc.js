module.exports = {
  'extends': 'airbnb-base',
  env: {
    browser: false,
    node: true,
    es6: true,
    mocha: true
  },
  settings: {
    'import/resolver': {
      'babel-module': {}
    }
  },
  rules: {
    'no-use-before-define': ['error', { 'functions': false, 'classes': true }],
		"comma-spacing": ["error", {
			"before": false,
			"after": true }],
    // 'valid-jsdoc': [
    //   'error',
    //   {
    //     requireReturn: true,
    //     requireReturnType: true,
    //     requireParamDescription: true,
    //     requireReturnDescription: true
    //   }
    // ],
    // 'require-jsdoc': [
    //   'error',
    //   {
    //     require: {
    //       FunctionDeclaration: true,
    //       MethodDefinition: true,
    //       ClassDeclaration: true
    //     }
    //   }
    // ]
  }
}
