/* eslint-disable no-underscore-dangle */
import _ from 'lodash';
import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';

export default function (config) {
  const opts = _.clone(config.jwt);
  opts.secretOrKey = config.secretOrKey;
  opts.jwtFromRequest = (request) => {
    if (request.query && request.query.jwt) {
      return ExtractJwt.fromUrlQueryParameter('jwt')(request);
    }
    return ExtractJwt.fromAuthHeaderWithScheme('Bearer')(request);
  };

  passport.use(new Strategy(opts, (jwtPayload, done) => done(
    null,
    {
      user: jwtPayload.sub,
    },
  )));

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });
  return passport;
}
