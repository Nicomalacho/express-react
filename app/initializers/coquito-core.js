import productionBuckets from '../../lib/coquito-core/buckets';
import services from '../../lib/coquito-core';

export default function (config) {
  // here you can change buckets implementation
  const buckets = productionBuckets(config);

  return services(buckets, config);
}
