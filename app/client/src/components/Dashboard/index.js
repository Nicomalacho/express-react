import React from 'react'
import { Typography, Paper, Avatar } from '@material-ui/core'
import { VerifiedUserOutlined } from '@material-ui/icons'
import withStyles from '@material-ui/core/styles/withStyles'
import storage from '../../storage';
import { withRouter } from 'react-router-dom'
import NavBar from '../NavBar';

const styles = theme => ({
  main: {    
    width: '100%',  
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  submit: {
    marginTop: theme.spacing(3),
  },
})

function Dashboard(props) {
  const { classes } = props


  const user = storage.getUser();
  if (!user || !user.token) {
    // not logged in
    props.history.replace('/login')
    return null
  }

  return (
    <main className={classes.main}>
      <NavBar onLogout={logout} />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <VerifiedUserOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Hello
				</Typography>
        <Typography component="h1" variant="h5">
          Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
				</Typography>
      </Paper>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <VerifiedUserOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Hello
				</Typography>
        <Typography component="h1" variant="h5">
          Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
				</Typography>
      </Paper>
    </main>
  )

  async function logout() {
    storage.logoutUser();
    props.history.push('/')
  }
}

export default withRouter(withStyles(styles)(Dashboard))