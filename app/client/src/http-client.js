import _ from 'lodash';
import storage from './storage';

class httpClient {
  constructor() {
    this.host = "/v1"
    this.headers = getHeaders();
    // this.token = storage.getUser().token || '';
    this.options = {
      method: "GET",
      headers: this.headers
    }
  }
  async login(emailAddress, password) {
    const path = '/login';
    const body = { emailAddress, password };
    const opt = _.merge(this.options, { method: "POST", body: JSON.stringify(body) })
    try {
      const user = await request(`${this.host}${path}`, opt);
      storage.setUser(user);
    } catch (e) {
      throw e;
    }
  }
}

async function request(url, options) {
  try {
    const data = await fetch(url, options);
    return await data.json();
  } catch (e) {
    throw e;
  }
}
function getHeaders(options = {}) {
  return _.merge({
    'Content-Type': 'application/json'
  }, options)
}

// function buildToken(token) {
//   return { 'Authorization': `Bearer ${token}` }
// }

export default new httpClient();