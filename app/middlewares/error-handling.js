export default function (appContext) {
  const isDev = appContext.config.environment === 'development';

  return [error404, isDev ? error500Dev : error500];
}

function error404(req, res, next) {
  const err = new Error(res.__('app.errors.not-found'));
  err.status = 404;
  next(err);
}

function error500(err, req, res, next) {
  const error = errorParse(err, res);
  res.status(err.validations ? 400 : err.status || 500);
  res.json({ error });
}

function error500Dev(err, req, res, next) {
  const error = errorParse(err, res);
  error.stack = err.stack;

  if (err.innerError) {
    error.innerError = err.innerError;
  }

  res.status(err.validations ? 400 : err.status || 500);
  res.json({ error });
}

function errorParse(err, res) {
  const code = err.code || err.status || 500;
  const message = code > 1300 ? res.__(err.message) : err.message;

  const error = {
    code: err.message,
    message,
  };

  if (err.validations) {
    error.validations = err.validations;
  }

  return error;
}
