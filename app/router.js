import { Router } from 'express';
import routing from 'express-declarative-routing';
import IndexController from './controllers';
import UserController from './controllers/user';
import SecurityController from './controllers/security';

export default (appContext) => {
  const router = Router();
  const routes = {};
  const { baseURL } = appContext.config.app;
  routes[baseURL] = [{
    _public: {
      get: IndexController.get,
      login: {
        post: SecurityController.login,
      },
      users: {
        get: UserController.get,
      }
    }
  }]
  routing.buildRoutes(router, routes);
  return router;
}