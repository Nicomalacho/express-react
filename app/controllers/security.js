import _ from 'lodash';
import jwt from 'jsonwebtoken';

export default {
  async login(req, res, next) {
    const { body, app } = req;
    const { emailAddress, password } = body;
    
    const { app: config } = app.locals.appContext.config;
    const service = app.locals.appContext.services.user;
    try {
      const user = await service.login({ emailAddress, password })
      const response = {
        token: createToken(user, config),
      }
      res.json(response);
    } catch (e) {
      next(e);
    }
  },
};

function createToken(user, config, expiresIn = null) {
  const jwtConfig = _.clone(config.jwt);
  jwtConfig.expiresIn = expiresIn || config.loginExpiresIn;
  jwtConfig.subject = user.id;

  return jwt.sign({}, config.secretOrKey, jwtConfig);
}